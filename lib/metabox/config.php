<?php 

add_action('cmb2_admin_init', 'comet_metabox');

function comet_metabox(){

	$comet_box = new_cmb2_box(array(
		'id'			=> 'comet-post-box',
		'object_types'	=> array('post'),
		'title'			=> __('Additional Field', 'comet'),
	));

	$comet_box->add_field(array(
		'id'	=> '_for-video',
		'type'	=> 'oembed',
		'name'	=> 'Video URL'
	));

	$comet_box->add_field(array(
		'id'	=> '_for-audio',
		'type'	=> 'oembed',
		'name'	=> 'Audio URL'
	));

	$comet_box->add_field(array(
		'id'	=> '_for-gallery',
		'type'	=> 'file_list',
		'name'	=> 'Gallery Images'
	));

	$sliders = new_cmb2_box(array(
		'title'			=> 'Additional Fields',
		'id'			=> 'additional-for-sliders',
		'object_types'	=> array('comet-slider')
	));

	$sliders->add_field(array(
		'name'			=> 'Subtitle',
		'id'			=> '_slider-subtitle',
		'type'			=> 'text'
	));

	$sliders->add_field(array(
		'name'			=> 'First Button Text',
		'id'			=> '_first-button-text',
		'type'			=> 'text'
	));

	$sliders->add_field(array(
		'name'					=> 'First Button type',
		'id'					=> '_first-button-type',
		'type'					=> 'select',
			'options'			=> array(
				'red'			=> 'Red Button',
				'transparent'	=>	'Transparent Button'	
			)
	));

	$sliders->add_field(array(
		'name'				=> 'First Button URL',
		'id'				=> '_first-button-url',
		'type'				=> 'text',
		
	));
}