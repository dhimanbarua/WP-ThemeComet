<?php 

?>
<div id="comments">
    <h5 class="upper"><?php comments_number(); ?></h5>
    <ul class="comments-list">
<?php
wp_list_comments(
	array(
		'callback' => 'my_comments_callback'
	));
?>
	</ul>
</div>

<?php

comment_form(); ?>