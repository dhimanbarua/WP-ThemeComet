<?php 


class comet_rightside_widgets extends WP_Widget{

	public function __construct(){

		parent:: __construct('comet-latest-post', 'Comet latest Post', array(
			'description' => 'Custome Latest Post Widget by comet theme'
		));
	}

	public function widget($instance, $count){ ?>
		<?php echo $instance['before_widget']; ?>
          <?php echo $instance['before_title']; ?>Latest Posts<?php echo $instance['after_title']; ?>

        <?php 

        	$posts = new WP_Query(array(
        		'post_type' 	=> 'post',
        		'posts_per_page'=> $count['post_count']
        	));
        ?>
          <ul class="nav">
          	<?php while($posts->have_posts()): $posts->the_post(); ?>
            <li>
            	<a href="<?php the_permalink(); ?>"><?php the_title(); ?><i class="ti-arrow-right"></i>
            		<?php if(!empty($count['date'])): ?>
            			<span><?php the_time('d M, Y'); ?></span>
            		<?php endif; ?>
            	</a>
            </li>
            
           <?php endwhile; ?>
          </ul>
          
           
        <?php echo $instance['after_widget']; ?>
	<?php }

	public function form($val){ ?>
			<p>
				<label for="<?php echo $this->get_field_id('title'); ?>">Title: </label>
				<input type="text" id="<?php echo $this->get_field_id('title'); ?>" class='widefat' name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $val['title']; ?>">
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('post_count'); ?>">Number of posts to show:</label>
				<input class="tiny-text" id="<?php echo $this->get_field_id('post_count'); ?>" name="<?php echo $this->get_field_name('post_count'); ?>" step="1" min="1" value="<?php echo $val['post_count']; ?>" size="3" type="number">
			</p>
			<p>
				<input type="checkbox" id="<?php echo $this->get_field_id('date'); ?>" name="<?php echo $this->get_field_name('date'); ?>" value="showdate" <?php if(!empty($val['date'])){echo "checked='checked'";} ?>>
				<label for="<?php echo $this->get_field_id('date'); ?>">Display post date?</label>
			</p>

	<?php }
}
add_action('widgets_init', 'latest_post_widget');

function latest_post_widget(){
	register_widget('comet_rightside_widgets');
}





