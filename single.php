<?php get_header(); ?>
<section class="page-title parallax">
      <div data-parallax="scroll" data-image-src="<?php echo get_template_directory_uri(); ?>/images/bg/18.jpg" class="parallax-bg"></div>
      <div class="parallax-overlay">
        <div class="centrize">
          <div class="v-center">
            <div class="container">
              <div class="title center">
                <h1 class="upper"><?php global $redux_comet; echo $redux_comet['blog-title']; ?><span class="red-dot"></span></h1>
                <h4><?php echo $redux_comet['blog-subtitle']; ?></h4>
                <hr>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section>
      <div class="container">
        <div class="col-md-8">
        <?php while(have_posts()): the_post(); ?>
          <article class="post-single">
            <div class="post-info">
              <h2><a href="#"><?php the_title(); ?></a></h2>
              <h6 class="upper"><span>By</span><a href="<?php the_author(); ?>"> <?php the_author(); ?></a><span class="dot"></span><span><?php the_time('d F Y'); ?></span><span class="dot"></span><a href="#" class="post-tag"><?php the_tags(); ?></a></h6>
            </div>
            
            <div class="post-media"><?php the_post_thumbnail(); ?>
              <?php 
                $video = get_post_meta(get_the_id(), '_for-video', true);
                $audio = get_post_meta(get_the_id(), '_for-audio', true);
                
                if(!empty($video)){
                  echo wp_oembed_get($video);

                }elseif(!empty($audio)){
                  echo wp_oembed_get($audio);

                }

                ?> 
                 
                
            </div>

            <div class="post-body">
              <?php the_content(); ?>
    			
          </article>
      <?php endwhile; ?>
      
      <?php comments_template(); ?> 
        
          
          
        </div>
        <?php get_sidebar(); ?>
      </div>
    </section>

<?php get_footer(); ?>

