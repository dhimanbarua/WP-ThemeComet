<?php 

/**
 * Comet Fifteen functions and definitions
 * Anybody can use the theme but he/she will have to maintain 	the GPL 2 licence
 * Here you will get all the functions of Comet Fifteen
 */



// gallery shortcode
if(file_exists(dirname(__FILE__). '/gallery.php')){
	require_once(dirname(__FILE__). '/gallery.php');
}
// widgets shortcode 
if(file_exists(dirname(__FILE__).'/widgets.php')){
	require_once(dirname(__FILE__).'/widgets.php');
}
if(file_exists(dirname(__FILE__).'/gallery.php')){
	require_once(dirname(__FILE__).'/gallery.php');
}
// Reduxe Core Framework
if(file_exists(dirname(__FILE__).'/lib/ReduxCore/framework.php')){
	require_once(dirname(__FILE__).'/lib/ReduxCore/framework.php');
}
if(file_exists(dirname(__FILE__).'/lib/sample/custom-config.php')){
	require_once(dirname(__FILE__).'/lib/sample/custom-config.php');
}
// CMB2 Box(Meta Box)
if(file_exists(dirname(__FILE__).'/lib/metabox/init.php')){
	require_once(dirname(__FILE__).'/lib/metabox/init.php');
}
if(file_exists(dirname(__FILE__).'/lib/metabox/config.php')){
	require_once(dirname(__FILE__).'/lib/metabox/config.php');
}
// Nav Walker
if(file_exists(dirname(__FILE__).'/custom_nav_walker.php')){
	require_once(dirname(__FILE__).'/custom_nav_walker.php');
}
if(file_exists(dirname(__FILE__).'/class_admin_panel_walker.php')){
	require_once(dirname(__FILE__).'/class_admin_panel_walker.php');
}

// shortcodes 
if(file_exists(dirname(__FILE__).'/shortcodes/shortcodes.php')){
	require_once(dirname(__FILE__).'/shortcodes/shortcodes.php');
}

// tgm plugin activation
if(file_exists(dirname(__FILE__).'/lib/plugins/required-plugins.php')){
	require_once(dirname(__FILE__).'/lib/plugins/required-plugins.php');
}










// theme setup functions 

add_action('after_setup_theme', 'cometfifteen_functions');

function cometfifteen_functions(){
	// Text Domain
	load_theme_textdomain('comet', get_template_directory().'/lang');

	// theme Supports
	add_theme_support('title-tag');
	add_theme_support('woocommerce');
	add_theme_support('post-thumbnails');
	add_theme_support('post-formats', array(
		'video',
		'audio',
		'standard',
		'gallery',
		'quote'
	));
	// portfolio section
	if(current_user_can('manage_options')){

		register_post_type('comet-portfolio', array(
			'labels'=> array(
				'name' 			=> __('Portfolio', 'comet'),
				'add_new' 		=> __('Add New Portfolio', 'comet'),
				'add_new_item' 	=> __('Add New Portfolio', 'comet')
			),
			'public' 	=> true,
			'supports'	=>	array('title', 'editor', 'thumbnail')
		));
	}

	register_taxonomy('comet-protfolio-category', 'comet-portfolio', array(
		'labels' => array(
			'name'			=> __('categories', 'comet'),
			'add_new'		=> __('Add New category', 'comet'),
			'add_new_item'	=> __('Add New category', 'comet'),
		),
		'public'		=> true,
		'hierarchical' 	=> true
	));

	// slider section
	register_post_type('comet-slider', array(
		'labels' => array(
			'name'			=> __('Slider', 'comet'),
			'add_new'		=> __('Add New Slider', 'comet'),
			'add_new_item'	=> __('Add New Slider', 'comet')
		),
		'public'	=> true,
		'supports'	=> array('title','editor','thumbnail')
	));


	register_nav_menu('main-menu', __('Main Menu', 'comet'));
	register_nav_menu('footer-menu', __('Footer Menu', 'comet'));

	global $wpdb;

$prefix = $wpdb->prefix;
$table = $prefix .'dhiman';
require_once( ABSPATH . 'wp-admin/includes/upgrade.php');
dbDelta("CREATE TABLE $table (id INT AUTO_INCREMENT, name varchar(250), UNIQUE KEY id (id))");
}
// Adding fonts
function get_comet_fonts(){
	$fonts = array();
	$fonts[] = 'Montserrat:400,700';
	$fonts[] = 'Raleway:300,400,500';
	$fonts[] = 'Halant:300,400';
	$comet_fonts = add_query_arg(array(
		'family' => urlencode(implode('|', $fonts)),
		'subset' => 'latin'
	), 'https://fonts.googleapis.com/css');
	
	return $comet_fonts;
}
// including the theme styles
add_action('wp_enqueue_scripts', 'cometfifteen_styles');

function cometfifteen_styles(){
	wp_enqueue_style('bundle', get_template_directory_uri().'/css/bundle.css');
	wp_enqueue_style('css', get_template_directory_uri().'/css/style.css');
	wp_enqueue_style('fonts', get_comet_fonts());
	wp_enqueue_style('stylesheet', get_stylesheet_uri());
	wp_enqueue_style('comment-reply');
}

// including the theme conditional scripts
add_action('wp_enqueue_scripts', 'conditional_scripts');

function conditional_scripts(){
	wp_enqueue_script('html5shim', 'http://html5shim.googlecode.com/svn/trunk/html5.js', array(), '', false);
	wp_script_add_data('html5shim', 'conditional', 'lt IE 9');

	wp_enqueue_script('respond', 'https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js', array(), '', false);
	wp_script_add_data('respond', 'conditional', 'lt IE 9');
	wp_enqueue_script('comment-reply');
}

// including the theme scripts
add_action('wp_enqueue_scripts', 'cometfifteen_scripts');

function cometfifteen_scripts(){
	wp_enqueue_script('bundle', get_template_directory_uri().'/js/bundle.js', array('jquery'), '', true);

	wp_enqueue_script('google-map', 'https://maps.googleapis.com/maps/api/js?v=3.exp', array('jquery'), '', true);
	
	wp_enqueue_script('main', get_template_directory_uri().'/js/main.js', array('jquery', 'bundle'), '', true);
}


add_action('widgets_init', 'comet_rightsidebar');

function comet_rightsidebar(){
	register_sidebar(array(
		'name' 			=> __('Right sidebar', 'comet'),
		'description'	=> __('You may add your right sidebar widgets here', 'comet'),
		'id' 			=> 'right-sidebar',
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h6 class="upper">',
		'after_title'	=> '</h6>'
	));
	register_sidebar(array(
		'name' 			=> __('Footer Sidebar', 'comet'),
		'description'	=> __('You may add your footer area widgets sidebar'),
		'id' 			=> 'footer-widget',
		'before_widget' => '<div class="col-sm-4"><div class="widget">',
		'after_widget'	=> '</div></div>',
		'before_title'	=> '<h6 class="upper">',
		'after_title'	=> '</h6>'
	));
	register_sidebar(array(
		'name' 			=> __('Footer Subscribe bar', 'comet'),
		'description'	=> __('You may add your footer area widget sidebar'),
		'id'			=> 'footer-subscribe-bar',
		'before_widget'	=> '<div class="widget">',
		'after_widget'	=> '</div>',
		'before_title'	=> '<h6 class="upper">',
		'after_title'	=> '</h6>'
	));
}

add_action('admin_print_scripts', 'comet_scripts', 1000);

function comet_scripts(){ ?>

	<?php if(get_post_type() == 'post') : ?>

		<script>
			jQuery(document).ready(function(){
				
		var id = jQuery('input[name="post_format"]:checked').attr('id');
			
				if(id == 'post-format-video'){
					jQuery('.cmb2-id--for-video').show();
				}else{
					jQuery('.cmb2-id--for-video').hide();
				}

				if(id == 'post-format-audio'){
					jQuery('.cmb2-id--for-audio').show();
				}else{
					jQuery('.cmb2-id--for-audio').hide();
				}

				if(id == 'post-format-gallery'){
					jQuery('.cmb2-id--for-gallery').show();
				}else{
					jQuery('.cmb2-id--for-gallery').hide();
				}

				jQuery('input[name="post_format"]').change(function(){
					jQuery('.cmb2-id--for-video').hide();
					jQuery('.cmb2-id--for-audio').hide();
					jQuery('.cmb2-id--for-gallery').hide();

					var id = jQuery('input[name="post_format"]:checked').attr('id');

					if(id == 'post-format-video'){
						jQuery('.cmb2-id--for-video').show();
					}else{
						jQuery('.cmb2-id--for-video').hide();
					}

					if(id == 'post-format-audio'){
						jQuery('.cmb2-id--for-audio').show();
					}else{
						jQuery('.cmb2-id--for-audio').hide();
					}

					if(id == 'post-format-gallery'){
						jQuery('.cmb2-id--for-gallery').show();
					}else{
						jQuery('.cmb2-id--for-gallery').hide();
					}
				});
			});
		</script>

	<?php endif; ?>
<?php  }


add_filter('wp_edit_nav_menu_walker', 'admin_menu_function');

function admin_menu_function(){
	return 'comet_admin_menu';
}

add_action('wp_update_nav_menu_item', 'comet_menu_data_update', 10, 2);

function comet_menu_data_update($menuid, $dbid){
	update_post_meta($dbid, '_comet_nav', $_REQUEST['submenu-type'][$dbid]);
}


register_activation_hook(__FILE__, 'flush_rewrite');

function flush_rewrite(){
	flush_rewrite_rules();
}


add_action('vc_before_init', 'set_as_theme_vc');

function set_as_theme_vc(){
	vc_set_as_theme();
}

vc_map(array(
	'name'		=> 'Comet Slider',
	'base'		=> 'comet-slider',
	'params'	=>	array(
		array(
			'type'	=> 'textfield'
		)
	)
));

/*=====================
	custom comment form
========================*/

add_filter('comment_form_default_fields', 'comet_comment_form');

function comet_comment_form( $default ){

	$default['author'] = '<div class="form-double">
                  <div class="form-group">
                    <input name="author" type="text" placeholder="Name" class="form-control">
                  </div>';
    $default['email'] = '<div class="form-group last">
                    <input name="email" type="text" placeholder="Email" class="form-control">
                  </div>
                </div>';
    $default['coment'] = '<div class="form-group">
                  <textarea name="comment" placeholder="Comment" class="form-control"></textarea>
                </div>';
    $default['address'] = '<div class="form-group">
                  <textarea name="address" placeholder="Address" class="form-control"></textarea>
                </div>';
    $default['url'] = '';

	return $default;
}

add_action('comment_form_defaults', 'comet_defaults_comment_form');

function comet_defaults_comment_form( $default_info ){

	if( !is_user_logged_in() ){
		$default_info['comment_field'] = '';
	}else{
		$default_info['comment_field'] = '<div class="form-group">
                  <textarea name="comment" placeholder="Comment" class="form-control"></textarea>
                </div>';
	}

	$default_info['submit_button'] = '<button type="submit" class="btn btn-color-out">Post Comment</button>';
	$default_info['submit_field'] = '<div class="form-submit text-right">%1$s %2$s</div>';
	$default_info['comment_notes_before'] = '';
	$default_info['title_reply'] = 'Leave a comment';
	$default_info['title_reply_before'] = '<h3 class="upper">';
	$default_info['title_reply_after'] = '</h3>';



	return $default_info;
}

add_action('comment_post', 'save_comment_meta_data');

function save_comment_meta_data($id){

	add_comment_meta($id, 'address_field', $_POST['address']);
}

add_action('add_meta_boxes_comment', 'address_field_add_comment_box');
function address_field_add_comment_box(){
	add_meta_box('address', 'Address', 'address_callback', 'comment', 'normal', 'high');
}

function address_callback( $comment ){
	$address = get_comment_meta($comment->comment_ID, 'address_field', true);
	?>
	<table class="form-table editcomment">
		<tbody>
			<tr>
				<td class="first"><label for="name">Address:</label></td>
				<td><input name="newcomment_address" size="30" value="<?php echo esc_attr( $address ); ?>" id="name" type="text"></td>
			</tr>
		</tbody>
	</table>
	<?php 
}

function my_comments_callback($comment, $arg, $depth){

		$GLOBALS['comment'] = $comment;
	?>
		<li id="comment-<?php comment_ID(); ?>">
            <div <?php comment_id('class'); ?>>
              <div class="comment-pic">
              	<?php 
              		echo get_avatar($comment, '', '', '',
              			array(
              				'class' => 'img-circle'
              			));
              	?>
              </div>
              <div class="comment-text">
                <h5 class="upper">
                	<?php 
                		echo get_comment_author_link(); 
                	?>
                </h5>
                <span class="comment-date">

                	Posted on <?php comment_date('d F'); ?> at <?php comment_time('g:i a')?>
                </span>
                <p>
                	<?php 
                		comment_text();
                	?>
                </p>
                <div class="reply">
                	<?php 
                		comment_reply_link(
                			array_merge($arg, array(
            					'depth' 	=> $depth,
            					'max_depth'	=> $arg['max_depth']
							))
                		);
                	?>
                </div>
              </div>
            </div>
        </li>
	<?php 
}

/*=====================
	comet wooCommerce
========================*/

add_filter('woocommerce_show_page_title', function(){
	return;
});



















